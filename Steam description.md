# Base16 Audiosurf 2
A set of themes for Audiosurf 2 that works with [base16](https://github.com/chriskempson/base16).

Source code is available at [nlicky/base16-audiosurf2 on GitLab](https://gitlab.com/nlicky/base16-audiosurf2).

**Note:** The themes for Base16 Audiosurf 2 were built using the base16 default colors in mind. Some themes may not follow the colors defined in the default scheme.

# Usage
By default, a copy of `base16-default-dark.lua` is stored in the root directory of `base16-audiosurf2/` (the skin folder) as `base16-audiosurf2.lua`.

To change skins, simply copy a theme that you want to use from the `themes/` directory to the root directory, and rename the copied theme file to `base16-audiosurf2.lua`.

# What is Base16?
From the Base16 project:

> An architecture for building themes based on carefully chosen syntax highlighting using a base of sixteen colors. Base16 provides a set of guidelines detailing how to style syntax and how to code a _builder_ for compiling Base16 _schemes_ and _templates_.

Previews of all the included schemes are available at the [base16 website](http://chriskempson.com/projects/base16).

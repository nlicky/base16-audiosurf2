-- base16-audiosurf2 (https://gitlab.com/nlicky/base16-audiosurf2)
-- Base16 Audiosurf 2 template by viad (https://steamcommunity.com/id/frazr_lzer/)
-- One Light scheme by Daniel Pfeifer (http://github.com/purpleKarrot)

-- Define colors.
color_background = {r=250, g=250, b=250}
color_comment = {r=160, g=161, b=167}
color_foreground = {r=56, g=58, b=66}
color_light_bg = {r=240, g=240, b=241}

color_violet = {r=166, g=38, b=164}
color_blue = {r=64, g=120, b=242}
color_green = {r=80, g=161, b=79}
color_yellow = {r=193, g=132, b=1}
color_red = {r=202, g=18, b=67}

-- Use vehicle when in Wakeboard mode.
jumping = false

function fif(test, if_true, if_false)
  if test then return if_true else return if_false end
end

hifi = GetQualityLevel() > 2 -- GetQualityLevel returns 1,2,or 3
function ifhifi(if_true, if_false)
  if hifi then return if_true else return if_false end
end

quality = GetQualityLevel4()
function ByQuality4(low,med,high,ultra)
        if quality < 2 then return low
        elseif quality <3 then return med
        elseif quality <4 then return high
        else return ultra end
end

function ifwakeboard(if_true, if_false) -- create this function to help when changing a setting when we're in wakeboard mode
        if jumping then return if_true else return if_false end
end

skinvars = GetSkinProperties()
trackWidth = skinvars["trackwidth"]
ispuzzle = skinvars.colorcount>1
fullsteep = jumping or skinvars.prefersteep or (not ispuzzle)

-- SetScene code taken from Dusk.
SetScene{
        glowpasses = 0, -- no glow effect needed
        glowspread = 0, -- no glow effect needed
        radialblur_strength = 0, -- no radial blur effect needed
        dynamicFOV = false, -- no dynamic field of view effect needed
        airdebris_count = 0, -- no decorative air particles needed
        minimap_colormode = "track", -- color the minimap the normal way

        use_intro_swoop_cam = false,
        hide_default_background_verticals = true,

        water = ifwakeboard(true, false), -- use the built-in digital water effect only in wakeboard modes
                towropes = ifwakeboard(true, false), -- use the towropes if we're in wakeboard mode
                watertint = {r=255,g=255,b=255,a=234},
                minwaterrandomdarkness = 0.95,
                watertype = 2,
                watertexture = "WaterCubesBlue_BlackTop_WhiteLowerTier.png",--texture used to color the dynamic "digital water" surface
                numvisiblewaterchunks = 8
}

-- Disable flashes as in Dusk.
SetBlockFlashes{
        sizescaler_missed = 0
}

if not jumping then
squareMesh = BuildMesh{
        recalculateNormalsEveryFrame=true,
        --splitVertices = true,
        --barycentricTangents = true,
        meshes={"squaremorph_baseline.obj", "squaremorph0.obj", "squaremorph1.obj", "squaremorph2.obj", "squaremorph3.obj", "squaremorph4.obj"}
}


        SetBlocks{
                  maxvisiblecount = fif(hifi,200,50),
                  colorblocks={
                          --mesh = "SingleLozenge.obj",
                          mesh = "NewBlock.obj",
                          --shader = fif(ispuzzle, "Diffuse", "Rim Light"),
                          --shader = fif(ispuzzle, "Diffuse", "VertexColorUnlitTinted"),
                          shader = "VertexColorUnlitTinted",
                          --shadersettings={_Brightness=1.42},
                          texture = "NewBlock.png",
                  height = 0,
                  float_on_water = false,
                  scale = {1,1,1}
                  },
                  greyblocks={
                          mesh = "NewBlock.obj",
                          shader = "VertexColorUnlitTinted",
                          --shadersettings={_Brightness=1.42},
                          texture = "NewBlock.png",
                          shadercolors = {_Color=color_comment}
                  },
                  powerups={--override the following objects in case the mod uses them
                          powerpellet={
                                  --mesh = squareMesh,
                                  mesh = "PowerStar.obj",
                                  --shader = fif(hifi,"MatCap/Vertex/Textured Lit Double", "MatCap/Vertex/PlainBright"),
                                  shader = "VertexColorUnlitTinted",
                                  --shadersettings = fif(hifi, {_Brightness=3.0}, {_Brightness=5}),
                                  --shadersettings={_Brightness=1.42},
                                  --textures = {_MainTex="White.png", _MatCap="matcapchrome.jpg"},
                                  texture = "NewBlock.png",
                                  scale = {1,1,1},
                                  --scale = {2.7,2.7,2.7},
                                  shadercolors = {_Color="highway", scaletype="intensity", minscaler=2, maxscaler=2.5},
                                  },

                          whiteblock={
                                  mesh = "NewBlock.obj",
                                  shader = "VertexColorUnlitTinted",
                                  --shadersettings={_Brightness=1.42},
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                  },

                          ghost={
                                  mesh = "NewBlock.obj",
                                  shader = "VertexColorUnlitTinted",
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                },

                          -- Multiplier meshes from Stadium skin.
                          x2={
                                  mesh = "x2.obj",
                                  shader = "VertexColorUnlitTinted",
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                  },
                          x3={
                                  mesh = "x3.obj",
                                  shader = "VertexColorUnlitTinted",
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                  },
                          x4={
                                  mesh = "x4.obj",
                                  shader = "VertexColorUnlitTinted",
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                  },
                          x5={
                                  mesh = "x5.obj",
                                  shader = "VertexColorUnlitTinted",
                                  texture = "NewBlock.png",
                                  shadercolors = {_Color=color_foreground}
                                  },
                  }
        }
end

SetPuzzleGraphics{
        usesublayerclone = false,
        puzzlematchmaterial = {shader="Unlit/Transparent",texture="tileMatchingBars.png",aniso=9},
        puzzleflyupmaterial = {shader="VertexColorUnlitTintedAddFlyup",texture="tileMatchingBars.png"},
        puzzlematerial = {shader="VertexColorUnlitTintedAlpha",texture="tilesSquare.png",texturewrap="clamp",aniso=9, usemipmaps="false",shadercolors={_Color={255,255,255,255}}}
}

if jumping then
        SetPlayer{
                --showsurfer = true,
                --showboard = true,
                cameramode = "first_jumpthird",
                --cameramode_air = "third",--"first_jumptrickthird", --start in first, go to third for jumps and tricks

                camfirst={ --sets the camera position when in first person mode. Can change this while the game is running.
                        pos={0,2.7,-3.50475},
                        rot={20.49113,0,0},
                        strafefactor = 1
                },
                camthird={ --sets the two camera positions for 3rd person mode. lerps out towards pos2 when the song is less intense
                        pos={0,2.7,-3.50475},
                        rot={20.49113,0,0},
                        strafefactor = 0.75,
                        pos2={0,2.8,-3.50475},
                        rot2={20.49113,0,0},
                        strafefactorFar = 1},
                surfer={ --set all the models used to represent the surfer
                        arms={
                                --mesh="arm.obj",
                                shader="RimLightHatchedSurfer",
                                shadercolors={
                                        _Color={colorsource="highway", scaletype="intensity", minscaler=3, maxscaler=6, param="_Threshold", paramMin=2, paramMax=2},
                                        _RimColor={0,63,192}
                                },
                                texture="FullLeftArm_1024_wAO.png"
                        },
                        board={
                                --mesh="wakeboard.obj",
                                shader=ifhifi("RimLightHatchedSurferExternal","VertexColorUnlitTinted"), -- don't use the transparency shader in lofi mode. less fillrate needed that way
                                renderqueue=3999,
                                shadercolors={ --each color in the shader can be set to a static color, or change every frame like the arm model above
                                        _Color={colorsource="highway", scaletype="intensity", minscaler=5, maxscaler=5},
                                        _RimColor={0,0,0}
                                },
                                shadersettings={
                                        _Threshold=11
                                },
                                texture="board_internalOutline.png"
                        },
                        body={
                                --mesh="surferbot.obj",
                                shader="RimLightHatchedSurferExternal", -- don't use the transparency shader in lofi mode. less fillrate needed that way
                                renderqueue=3000,
                                layer = 14,
                                shadercolors={
                                        _Color={colorsource="highway", scaletype="intensity", minscaler=3, maxscaler=3},
                                        _RimColor={0,0,0}
                                },
                                shadersettings={
                                        _Threshold=1.7
                                },
                                texture="robot_HighContrast.png"
                        }
                }
        }
else
        SetPlayer{
                cameramode = "third",
                cameradynamics = "high",

                camfirst={
                        pos={0,1.84,-0.8},
                        rot={20,0,0}},
                camthird={
                        --pos={0,2.1,-0.5},
                        --pos={0,2,-0.5},
                        pos={0,2.5,-1},
                        rot={30,0,0},
                        strafefactor = .5,
                        strafefactorFar = .75,
                        pos2={0,5,-4},
                        --pos2={0,2.5,-1},
                        rot2={30,0,0},
                        --strafefactorFar = 1,
                        --transitionspeed = 3,
                        transitionspeed = 3,
                        puzzleoffset=-0.95,
                        puzzleoffset2=-1.5},
                vehicle={ --livecoding not supported here
                        min_hover_height= 0.05,
                        max_hover_height = 0.9,
                        use_water_rooster = false,
                        smooth_tilting = false,
                        smooth_tilting_speed = 10,
                        smooth_tilting_max_offset = -20,
                        pos={x=0,y=0,z=0},
                        mesh="ninjamono.obj",
                        --shader="VertexColorUnlitTintedPixelate",
                        shader="VertexColorUnlitTinted",
                        --layer = ifhifi(14,15), -- in hifif mode, render it to the topmost only. so the non-pixelated parts don't show through
                        layer = 15,
                        renderqueue = 2000,
                        shadersettings={_Scale=8, _Brightness=1.5},
                        shadercolors={
                                _Color = {colorsource="highway", scaletype="intensity", minscaler=1, maxscaler=1.5},
                                _RimColor = {43,43,43}},
                        texture="ninjamono.png",
                        scale = {x=1,y=1,z=1},
                        thrusters = {crossSectionShape={ {-.35,-.35,0},{-.5,0,0},{-.35,.35,0},{0,.5,0},{.35,.35,0},{.5,0,0},{.35,-.35,0}},
                                                perShapeNodeColorScalers={.5,1,1,1,1,1,.5},
                                                --shader="VertexColorUnlitTinted",
                                                shader="VertexColorUnlitTintedAddSmooth",
                                                layer = 14,
                                                renderqueue = 2999,
                                                colorscaler = 2,
                                                extrusions=22,
                                                stretch=-0.1191,
                                                updateseconds = 0.025,
                                                instances={
                                                        {pos={0,.46,-1.28},rot={0,0,0},scale={.7,.7,.7}},
                                                        {pos={.175,0.21,-1.297},rot={0,0,58.713},scale={.7,.7,.7}},
                                                        {pos={-.175,0.21,-1.297},rot={0,0,313.7366},scale={.7,.7,.7}}
                                                }}
                }
        }
end

SetTrackColors{ --enter any number of colors here. The track will use the first ones on less intense sections and interpolate all the way to the last one on the most intense sections of the track
    color_violet,
    color_blue,
    color_green,
    color_yellow,
    color_red
}

if skinvars.colorcount < 5 then
        SetBlockColors{
            color_blue,
            color_green,
            color_yellow,
            color_red
        }
else
        SetBlockColors{
            color_violet,
            color_blue,
            color_green,
            color_yellow,
            color_red
        }
end

-- Ring size adjustments from Stadium skin.
extraWidth = trackWidth - 5

SetRings{ --setup the tracks tunnel rings. the airtexture is the tunnel used when you're up in a jump
        texture = ByQuality4("viad_Ring_256.png", "viad_Ring_512.png", "viad_Ring_1024.png", "viad_Ring_1024.png"),
        --texture="Classic_OnBlack",
        --shader="VertexColorUnlitTintedAddSmooth",
        shader = "VertexColorUnlitTintedAddDouble",
        layer = 13, -- on layer13, these objects won't be part of the glow effect
        size=fif(jumping, trackWidth*2, 7 + extraWidth), --22
        offset = fif(jumping, {0,0,0}, {0,1,0}),
        percentringed=.2,--ifhifi(2,.01),-- .2,

        -- Disable air rings.
        airtexture="Bits.png",
        airshader="VertexColorUnlitTintedAddSmoothNoDepth",
        airsize=0,
        useairrings = false
}

SetWake{
        height = 0 -- no wake. not even in wakeboard mode
}

track = GetTrack()--get the track data from the game engine

--RAILS. rails are the bulk of the graphics in audiosurf. Each one is a 2D shape extruded down the length of the track.

if not jumping then
        local laneDividers = skinvars["lanedividers"]
        for i=1,#laneDividers do
                CreateRail{ -- lane line
                        positionOffset={
                                x=laneDividers[i],
                                y=0.1},
                        crossSectionShape={
                                {x=-.07,y=0},
                                {x=.07,y=0}},
                        perShapeNodeColorScalers={
                                1,
                                1},
                        colorMode="static",
                        color = color_comment,
                        flatten=false,
                        nodeskip = 2,
                        wrapnodeshape = false,
                        shader="VertexColorUnlitTinted"
                }
        end
end

-- Affects both gray and white blocks, grid markings, and the road.
if not jumping then
        CreateRail{--road surface
                positionOffset={
                        x=0,
                        y=0},
                crossSectionShape={
                        {x=-trackWidth,y=0},
                        {x=trackWidth,y=0}},
                perShapeNodeColorScalers={
                        1,
                        1},
                colorMode="static",
                color = color_light_bg,
                --renderqueue=3000,
                renderqueue = 2001,
                flatten=false,
                shader="VertexColorUnlitTintedAlpha"
        }
end

CreateRail{--left rail
        positionOffset={
                x=-trackWidth - .2,
                y=0},
        crossSectionShape={
                {x=-.65,y=0.5},
                {x=.3,y=0.5},
                {x=.3,y=-3},
                {x=-.65,y=-3}},
        perShapeNodeColorScalers={
                .5,
                1,
                0,
                0},
        colorMode="highway",
        color = color_comment,
        flatten=false,
        shader="ProximityEmissionVertexColor",
        shadercolors={_Color={r=0,g=0,b=0}},
        shadersettings={_StartDistance=3.92, _FullDistance=11.9}
}

CreateRail{--right rail
        positionOffset={
                x=trackWidth + .2,
                y=0},
        crossSectionShape={
                {x=-.65,y=0.5},
                {x=.3,y=0.5},
                {x=.3,y=-3},
                {x=-.65,y=-3}},
        perShapeNodeColorScalers={
                .5,
                1,
                0,
                0},
        colorMode="highway",
        color = color_comment,
        flatten=false,
        shader="ProximityEmissionVertexColor",
        shadercolors={_Color={r=0,g=0,b=0}},
        shadersettings={_StartDistance=3.92, _FullDistance=11.9}
}

CreateRail{--left rail edge
        positionOffset={
                x=-trackWidth - .2,
                y=0},
        crossSectionShape={
                {x=.28,y=0.51},
                {x=.31,y=0.51},
                {x=.31,y=0.48}},
        colorMode="static",
        color = color_comment,
        flatten=false,
        shader="VertexColorUnlitTinted",
        shadercolors={_Color={r=0,g=0,b=0}}
}

CreateRail{--right rail edge
        positionOffset={
                x=trackWidth + .2,
                y=0},
        crossSectionShape={
                {x=-.66,y=0.48},
                {x=-.66,y=0.51},
                {x=-.63,y=0.51}},
        colorMode="static",
        color = color_comment,
        flatten=false,
        shader="VertexColorUnlitTinted",
        shadercolors={_Color={r=0,g=0,b=0}}
}

if jumping then
        CreateRail{--left aurora core
                positionOffset={
                        x=-18,
                        y=33},
                crossSectionShape={
                        {x=-13,y=.5},
                        {x=-9,y=.5},
                        {x=-9,y=-.5},
                        {x=-13,y=-.5}},
                perShapeNodeColorScalers={
                        1,
                        1,
                        1,
                        1},
                colorMode="highway",
                color = color_comment,
                flatten=true,
                texture="White.png",
                shader="VertexColorUnlitTinted"
        }

        CreateRail{--right aurora core
                positionOffset={
                        x=18,
                        y=33},
                crossSectionShape={
                        {x=9,y=.5},
                        {x=13,y=.5},
                        {x=13,y=-.5},
                        {x=9,y=-.5}},
                perShapeNodeColorScalers={
                        1,
                        1,
                        1,
                        1},
                colorMode="highway",
                color = color_comment,
                flatten=true,
                texture="White.png",
                shader="VertexColorUnlitTinted"
        }

        auroraExtent = ifhifi(77, 22)
        CreateRail{--left aurora
                positionOffset={
                        x=-18,
                        y=33},
                crossSectionShape={
                        {x=-auroraExtent,y=.5},
                        {x=-11,y=.5},
                        {x=-11,y=-.5},
                        {x=-auroraExtent,y=-.5}},
                perShapeNodeColorScalers={
                        0,
                        1,
                        1,
                        0},
                colorMode="aurora",
                color = color_comment,
                flatten=true,
            layer=13,
                texture="White.png",
                shader="VertexColorUnlitTintedAddSmoothNoCull"
        }

        CreateRail{--right aurora
                positionOffset={
                        x=18,
                        y=33},
                crossSectionShape={
                        {x=11,y=.5},
                        {x=auroraExtent,y=.5},
                        {x=auroraExtent,y=-.5},
                        {x=11,y=-.5}},
                perShapeNodeColorScalers={
                        1,
                        0,
                        0,
                        1},
                colorMode="aurora",
                color = color_comment,
                flatten=true,
            layer=13,
                texture="White.png",
                shader="VertexColorUnlitTintedAddSmoothNoCull"
        }
end

-- Set a solid color for the background.
SetBackgroundColor(color_background)

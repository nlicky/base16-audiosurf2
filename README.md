# Base16 Audiosurf 2
A set of themes for [Audiosurf 2](http://audiosurf2.com/) that works with [base16](https://github.com/chriskempson/base16).

Audiosurf 2 mods can be found on the [Steam Workshop](https://steamcommunity.com/id/frazr_lzer/myworkshopfiles/?appid=235800).

The themes were built using [base16-builder-python](https://github.com/InspectorMustache/base16-builder-python).

**Note:** The template for Base16 Audiosurf 2 was built using the base16 default colors in mind. Some themes may not follow the colors defined in the default scheme.

# Usage
Refer to the [Steam description](Steam description.md) for using Base16 Audiosurf 2 in the game.

# Packaging
1. Update the base16 schemes and build using the base16-audiosurf2 template as necessary.
2. Merge the contents of `b16-as2 Assets.zip` (found in this project's Wiki) with this repository. Make sure that the name of the root directory is `base16-audiosurf2/`.
3. Copy `base16-default-dark.lua` from `themes/` into the root directory, and rename the file to `base16-audiosurf2.lua`.
